# forms.py
from django import forms
from django.core.mail import send_mail


class AccountActionForm(forms.Form):
    comment = forms.CharField(
        required=False,
        widget=forms.Textarea,
    )
    send_email = forms.BooleanField(
        required=False,
    )

    @property
    def email_subject_template(self): return 'email/account/notification_subject.txt'

    @property
    def email_body_template(self): raise NotImplementedError()

    def form_action(self, account, user): raise NotImplementedError()

    def save(self, account, user):
        try:
            account, action = self.form_action(account, user)
        except Exception as e:
            error_message = str(e)
            self.add_error(None, error_message)
            raise

        if self.cleaned_data.get('send_email', False):
            send_mail(
                'Subject here',
                'Here is the message.',
                'from@example.com',
                ['to@example.com'],
                fail_silently=False,
            )
            return account, action
