from django.contrib.admin.filters import RelatedFieldListFilter
from django.db.models import Q, Count
from django.utils.translation import gettext_lazy as _
from django.template.defaultfilters import slugify
from django.utils.encoding import force_text
from django.utils.timezone import now as timezone_now
from datetime import timedelta, datetime
from rangefilter.filter import DateRangeFilter as DateRangeFilterOrig

from .choice_fields import Status


class RelatedDropdownFilter(RelatedFieldListFilter):
    template = 'admin/crm/accountselling/dropdown_filter.html'


class DateRangeFilter(DateRangeFilterOrig):
    template = 'admin/crm/account/date_filter.html'


class CountingByDateFilter(DateRangeFilter):
    title = _('Date Filter')
    template = 'admin/crm/userreport/date_filter.html'

    def queryset(self, request, queryset):
        if self.form.is_valid():
            validated_data = dict(self.form.cleaned_data.items())
            if validated_data:
                date_limits = self._make_query_filter(request, validated_data)
                print('DATE_LIMITS', date_limits)
                queryset = queryset.annotate(
                    _acc_all=Count('account', filter=Q(**date_limits)),
                    _acc_at_verification=Count('account',
                                               filter=Q(account__status=Status.AT_VERIFICATION.value, **date_limits)),
                    _acc_banned=Count('account', filter=Q(account__status=Status.BANNED.value, **date_limits)),
                    _acc_sold=Count('account', filter=Q(account__status=Status.SOLD.value, **date_limits)),
                )
        return queryset

    @staticmethod
    def _get_lookup_choices():
        day = timedelta(days=1)
        today = timezone_now().date()
        tomorrow = today + day
        yesterday = today - day
        current_week_begin = today - timedelta(days=today.weekday())
        current_week_end = current_week_begin + timedelta(days=6)
        current_month_begin = datetime(today.year, today.month, 1)
        current_mongth_end = datetime(today.year + 1, 1, 1) \
            if today.month >= 12 else datetime(today.year, today.month + 1, 1)
        current_year_begin = datetime(today.year, 1, 1)
        current_year_end = datetime(today.year + 1, 1, 1)

        return (
            ('today', _('Today'), today, tomorrow),
            ('yesterday', _('Yesterday'), yesterday, today),
            ('current_week', _('Current week'), current_week_begin, current_week_end),
            ('current_month', _('Current month'), current_month_begin, current_mongth_end),
            ('current_year', _('Current year'), current_year_begin, current_year_end),
        )

    def choices(self, cl):
        query_string_real = cl.get_query_string()
        query_string = cl.get_query_string({}, remove=self._get_expected_fields())
        hrefs_qs_template = query_string + self.lookup_kwarg_gte + '={0}' + '&' + self.lookup_kwarg_lte + '={1}'
        hrefs = []
        for lookup, title, begin_date, end_date in self._get_lookup_choices():
            hrefs_qs = hrefs_qs_template.format(begin_date.strftime("%d.%m.%Y"), end_date.strftime("%d.%m.%Y"))
            print(lookup, query_string_real, hrefs_qs, query_string_real == hrefs_qs)
            hrefs.append({
                'selected': hrefs_qs == query_string_real,
                'query_string': hrefs_qs,
                'display': title,
            })
        yield {
            'system_name': force_text(slugify(self.title) if slugify(self.title) else id(self.title)),
            'query_string': cl.get_query_string(
                {}, remove=self._get_expected_fields()
            ),
            'hrefs': hrefs,
        }
