from django.utils.translation import gettext_lazy as _
from enum import IntEnum


class StatusEnum(IntEnum):
    def __repr__(self): return self.value

    def __str__(self): return str(self.value)


class Status(StatusEnum):
    AT_VERIFICATION = 1
    READY = 2
    DISMISSED = 3
    SOLD = 4
    SENT_VIP = 5
    BANNED = 6
    WAIT_VIP = 7
    REMOVED = 10
    OTHER = 20


STATUS_CHOICES = (
    (Status.OTHER.value, _('Other')),
    (Status.AT_VERIFICATION.value, _('At verification')),
    (Status.READY.value, _('Ready')),
    (Status.DISMISSED.value, _('Dismissed')),
    (Status.SOLD.value, _('Sold')),
    (Status.SENT_VIP.value, _('Sent VIP')),
    (Status.BANNED.value, _('Banned')),
    (Status.WAIT_VIP.value, _('Wait VIP')),
    (Status.REMOVED.value, _('Removed')),
)


COUNTRY_CHOICES = (
    ('ru', _('Russia')),
    ('kz', _('Kazakstan')),
    ('rb', _('Belorussia')),
    ('am', _('Armenia')),
    ('md', _('Moldova')),
    ('ge', _('Georgia')),
    ('in', _('India')),
    ('vn', _('Vietnam')),
    ('o', _('Other')),
)

CURRENCY_CHOICES = (
    ('dollar', _('Dollar')),
    ('euro', _('Euro')),
    ('ruble', _('Ruble')),
)


class DocStatus(StatusEnum):
    CREATED = 10  # Документ создан, назначьте листовщика.
    LISTOVSHIK = 20  # Листовщик назначен.
    NEED_REGISTROVSHIK = 30  # Файлы прикреплены, отправить регистровщику.
    REGISTROVSHIK = 40  # Регистровщик назначен.
    ACCOUNT_CREATED = 50  # Аккаунты созданы. Проверка.
    DONE = 50  # Завершено.


DOCUMENT_STATUS_CHOICES = (
    (DocStatus.CREATED.value, _('Document created, need to add listovshik')),
    (DocStatus.LISTOVSHIK.value, _('Listovshik assigned')),
    (DocStatus.NEED_REGISTROVSHIK, _('Need to assign registrovshik')),
    (DocStatus.REGISTROVSHIK, _('Registrovshik assigned')),
    (DocStatus.ACCOUNT_CREATED, _('Account was created')),
    (DocStatus.DONE, _('Document job was finished')),
)


class DocType(StatusEnum):
    ORIGINAL = 10
    DOUBLE = 20


DOCUMENT_TYPE_CHOICES = (
    (DocType.ORIGINAL.value, _('Original')),
    (DocType.DOUBLE.value, _('Double'))
)
