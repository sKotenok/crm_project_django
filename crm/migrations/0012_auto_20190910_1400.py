# Generated by Django 2.2.4 on 2019-09-10 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0011_auto_20190830_1056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='status',
            field=models.PositiveSmallIntegerField(choices=[(20, 'Other'), (1, 'At verification'), (2, 'Ready'), (3, 'Dismissed'), (4, 'Sold'), (5, 'Sent VIP'), (6, 'Banned'), (7, 'Wait VIP'), (10, 'Removed')], default=1, verbose_name='Status'),
        ),
    ]
