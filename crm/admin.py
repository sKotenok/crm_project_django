import copy
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import User
# from django.contrib.messages import ERROR
from django.db import models
from django.db.models import Q, Count
from django.http import HttpResponseRedirect, JsonResponse
# from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse, path, resolve
from django.shortcuts import render
from django.utils.html import format_html
from django.utils.text import format_lazy
from django.utils.timezone import localtime
from django.utils.translation import gettext_lazy as _
# from django.utils.functional import curry

from .models import Account, AccountSelling, SoldAccount
from .models import Document, DocumentImage  # , DocumentStatusHistory
from .filters import RelatedDropdownFilter, CountingByDateFilter, DateRangeFilter
from .choice_fields import Status, DocStatus
from .utils import send_mail_to_customer


admin.site.site_header = _('CRM admin')
admin.site.site_title = _('CRM admin portal')
admin.site.index_title = _('Welcome to our CRM')
admin.site.register(LogEntry)


# TODO: Move to widgets!!!
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper


class AccountAddWidget(RelatedFieldWidgetWrapper):
    template_name = 'admin/crm/document/widgets/related_widget_wrapper.html'

    def __init__(self, widget, rel, admin_site, can_add_related=None,
                can_change_related=False, can_delete_related=False,
                can_view_related=False, object_id=None):
        super(AccountAddWidget, self).__init__(widget, rel, admin_site, can_add_related=can_add_related,
                can_change_related=can_change_related, can_delete_related=can_delete_related,
                can_view_related=can_view_related)
        self.object_id = object_id

    def get_context(self, name, value, attrs):
        from django.contrib.admin.views.main import IS_POPUP_VAR, TO_FIELD_VAR
        rel_opts = self.rel.model._meta
        info = (rel_opts.app_label, rel_opts.model_name)
        self.widget.choices = self.choices
        url_params = '&'.join("%s=%s" % param for param in [
            (TO_FIELD_VAR, self.rel.get_related_field().name),
            (IS_POPUP_VAR, 1),
        ])
        if self.object_id:
            url_params += '&document_id={0}'.format(self.object_id)
        context = {
            'rendered_widget': self.widget.render(name, value, attrs),
            'is_hidden': self.is_hidden,
            'name': name,
            'url_params': url_params,
            'model': rel_opts.verbose_name,
            'can_add_related': self.can_add_related,
            'can_change_related': self.can_change_related,
            'can_delete_related': self.can_delete_related,
            'can_view_related': self.can_view_related,
        }
        if self.can_add_related:
            context['add_related_url'] = self.get_related_url(info, 'add')
        if self.can_delete_related:
            context['delete_related_template_url'] = self.get_related_url(info, 'delete', '__fk__')
        if self.can_view_related or self.can_change_related:
            context['change_related_template_url'] = self.get_related_url(info, 'change', '__fk__')
        return context


class AccountAdmin(admin.ModelAdmin):
    """All the admin interface for question model with a lot of customization."""

    # Options for add/edit exact account page
    fieldsets = [
        (_('Mail'), {'fields': (
            'mail_email',
            ('mail_name', 'mail_phone'),
            ('mail_login', 'mail_password'),
            'mail_question',
        )}),
        (_('Neteller'), {'fields': (
            ('neteller_id', 'neteller_email'),
            ('neteller_login', 'neteller_password'),
            'neteller_code',
        )}),
        (_('Bet'), {'fields': (
            ('bet_name', 'bet_date'),
            ('bet_login', 'bet_password'),
            'bet_pin',
            ('bet_country', 'bet_currency'),
        )}),
        (_('Proxy'), {'fields': (
            ('proxy_ip', 'proxy_port'),
            'proxy_proxy',
            ('proxy_login', 'proxy_password'),
            ('proxy_created_at', 'proxy_duration'),
        )}),
        (None, {'fields': (('document_href', 'document_date'), 'notes', 'status', ('created_by', 'created_at'),)})
    ]
    readonly_fields = ('created_by', 'created_at', 'document_href', 'document_date')
    save_as = True
    save_on_top = True

    # Options for account list page
    list_display = ('id', 'mail_name', 'status', 'bet_country', 'bet_currency', 'mail_email', 'proxy_remain_days')
    list_filter = (
        ('created_at', DateRangeFilter),
        'status',
        'bet_country',
        'bet_currency',
        ('created_by', RelatedDropdownFilter),
    )
    date_hierarchy = 'created_at'
    list_display_links = ('id', 'mail_name')
    list_per_page = settings.ROWS_ON_EACH_PAGE
    search_fields = ('bet_name', 'mail_name', 'mail_email', 'neteller_email',
                    'mail_login', 'neteller_login', 'bet_login', 'notes')
    # sortable_by = ('id', 'bet_name', 'status', 'bet_country', 'mail_email')
    ordering = ('-created_at', )
    actions_on_top = False
    actions_on_bottom = True
    show_full_result_count = False
    view_on_site = False

    def get_readonly_fields(self, request, obj=None):
        fields = super(AccountAdmin, self).get_readonly_fields(request, obj)
        fields = self._get_readonly_fields_by_permissions(obj, fields, request)
        return fields

    def _get_readonly_fields_by_permissions(self, obj, fields, request):
        user = request.user
        if user.is_superuser:
            return fields
        if not obj:  # On account add
            return fields + ('status', )
        if user.has_perm('crm.cant_change_account_status'):
            return fields + ('status', )
        return fields

    def get_queryset(self, request):
        queryset = super(AccountAdmin, self).get_queryset(request)
        queryset = self._filter_queryset_by_permissions(queryset, request)
        return queryset

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super(AccountAdmin, self).get_search_results(request, queryset, search_term)
        queryset = self._filter_queryset_by_permissions(queryset, request)
        return queryset, use_distinct

    def _filter_queryset_by_permissions(self, queryset, request):
        user = request.user
        if user.is_superuser or user.has_perm('crm.view_account'):
            return queryset
        if user.has_perm('crm.view_account_owned_only'):
            queryset = queryset.filter(created_by=user.id)
        if user.has_perm('crm.view_account_not_sold'):
            queryset = queryset.filter(status__not=Status.SOLD.value)
        return queryset

    def has_view_permission(self, request, obj=None):
        user = request.user
        owned_only = user.has_perm('crm.view_account_owned_only')
        not_sold = user.has_perm('crm.view_account_not_sold')
        if obj:
            owned_only = owned_only and obj.created_by == user.id
            not_sold = not_sold and obj.status != Status.SOLD.value
        return super(AccountAdmin, self).has_view_permission(request, obj) or owned_only or not_sold

    def has_change_permission(self, request, obj=None):
        user = request.user
        owned_only = user.has_perm('crm.change_account_owned_only')
        not_sold = user.has_perm('crm.change_account_not_sold')
        at_verif = user.has_perm('crm.change_account_at_verification_only')
        if obj:
            owned_only = owned_only and obj.created_by == user.id
            not_sold = not_sold and obj.status != Status.SOLD.value
            at_verif = at_verif and obj.status == Status.AT_VERIFICATION.value
        return super(AccountAdmin, self).has_change_permission(request, obj) or owned_only or not_sold or at_verif

    def get_actions(self, request):
        """Conditional actions. Only superuser can delete accounts from database."""
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    def get_form(self, request, obj=None, **kwargs):
        form = super(AccountAdmin, self).get_form(request, obj, **kwargs)
        document_id = request.GET.get('document_id', None)
        if document_id:
            document = Document.objects.get(id=int(document_id))
            for name in ('mail_name', 'bet_name'):
                form.base_fields[name].initial = document.name
            form.base_fields['bet_country'].initial = document.bet_country
        return form

    def save_model(self, request, obj, form, change):
        """Assign current user on save"""
        obj.created_by = request.user
        super(AccountAdmin, self).save_model(request, obj, form, change)

    def proxy_remain_days(self, obj):
        if not obj.proxy_created_at:
            return None
        remain_delta = obj.proxy_created_at - localtime().date()
        rd_days = obj.proxy_duration - abs(remain_delta.days)
        span_title = format_lazy(_('Was registered at {0}'), obj.proxy_created_at.strftime("%d.%m.%Y"))
        color_class = 'default'
        if rd_days < 0:
            rd_days = '--'
            color_class = 'lost'
        elif rd_days <= settings.PROXY_DURATION_CRITICAL:
            span_title += _('. Life time is critically low, prolong it immediately!')
            color_class = 'critical'
        elif rd_days <= settings.PROXY_DURATION_WARNING:
            color_class = 'warning'
        return format_html('<span class="proxy-duration-{0}" title="{1}">{2}</span>', color_class, span_title, rd_days)
    proxy_remain_days.admin_order_field = 'proxy_created_at'
    proxy_remain_days.short_description = _('Proxy remain (days)')

    def document_href(self, obj):
        url = reverse('admin:crm_document_change', kwargs={'object_id': obj.document.id})
        return format_html('<a href="{url}">{name}</a>',
            url=url, name=url
        ) if obj and hasattr(obj, 'document') else '-'

    def document_date(self, obj):
        return obj.document.created_at.strftime("%d.%m.%Y") if obj and hasattr(obj, 'document') else '-'

    def get_row_css(self, obj, index):
        return Status(obj.status).name


admin.site.register(Account, AccountAdmin)


class AccountSellingAdmin(admin.ModelAdmin):
    view_on_site = False
    actions_on_top = False
    actions_on_bottom = False
    save_as = True
    list_per_page = settings.ROWS_ON_EACH_PAGE
    readonly_fields = ('sended', 'created_by', 'created_at')

    def get_fields(self, request, obj=None):
        fields = ('customer_email', 'count', 'bet_country', 'currency')
        if obj:
            return fields + self.readonly_fields
        else:
            return fields

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        if obj.sended:
            super().save_model(request, obj, form, change)
        else:
            # Getting ready to sell accounts from database
            accounts = Account.objects.filter(
                status=Status.READY.value,
                bet_currency=obj.currency,
                bet_country=obj.bet_country
            )[0:obj.count]

            obj.sended = True
            super().save_model(request, obj, form, change)

            for acc in accounts:
                acc.status = Status.SOLD.value
                acc.save(update_fields=('status',))

                sold_acc = SoldAccount(account=acc, selling=obj)
                sold_acc.save()
            send_mail_to_customer(obj, accounts, request)


admin.site.register(AccountSelling, AccountSellingAdmin)


class UserReport(User):
    class Meta:
        proxy = True
        verbose_name = _('Account report')
        verbose_name_plural = _('Account reports')


class UserReportAdmin(admin.ModelAdmin):
    # Options for account list page
    list_display = ('id', 'username', 'fullname', 'acc_all', 'acc_at_verification', 'acc_banned', 'acc_sold')
    list_filter = (
        ('account__created_at', CountingByDateFilter),
    )
    list_per_page = settings.ROWS_ON_EACH_PAGE
    ordering = ('username', )
    actions_on_top = False
    actions_on_bottom = False
    view_on_site = False

    def get_queryset(self, request):
        queryset = super(UserReportAdmin, self).get_queryset(request)

        queryset = queryset.annotate(
            _acc_all=Count('account'),
            _acc_at_verification=Count('account', filter=Q(account__status=Status.AT_VERIFICATION.value)),
            _acc_banned=Count('account', filter=Q(account__status=Status.BANNED.value)),
            _acc_sold=Count('account', filter=Q(account__status=Status.SOLD.value)),
       )
        return queryset

    def has_add_permission(self, request): return False
    def has_delete_permission(self, request, obj=None): return False
    def has_change_permission(self, request, obj=None): return False

    def fullname(self, obj): return obj.last_name + ' ' + obj.first_name
    fullname.admin_order_field = 'last_name'
    fullname.short_description = _('Full name')

    def acc_all(self, obj):
        url = '{}?created_by__id__exact={}'.format(reverse('admin:crm_account_changelist'), obj.id)
        return format_html('<a href="{}">{}</span>', url, obj._acc_all)
    acc_all.short_description = _('Report: All Registered')
    acc_all.admin_order_field = '_acc_all'

    def acc_at_verification(self, obj):
        url = '{}?created_by__id__exact={}&status__exact={}'.format(
            reverse('admin:crm_account_changelist'), obj.id, Status.AT_VERIFICATION.value)
        return format_html('<a href="{}">{}</span>', url, obj._acc_at_verification)
    acc_at_verification.short_description = _('Report: At verification')
    acc_at_verification.admin_order_field = '_acc_at_verification'

    def acc_banned(self, obj):
        url = '{}?created_by__id__exact={}&status__exact={}'.format(
            reverse('admin:crm_account_changelist'), obj.id, Status.BANNED.value)
        return format_html('<a href="{}">{}</span>', url, obj._acc_banned)
    acc_banned.short_description = _('Report: Banned')
    acc_banned.admin_order_field = '_acc_banned'

    def acc_sold(self, obj):
        url = '{}?created_by__id__exact={}&status__exact={}'.format(
            reverse('admin:crm_account_changelist'), obj.id, Status.SOLD.value)
        return format_html('<a href="{}">{}</span>', url, obj._acc_sold)
    acc_sold.short_description = _('Report: Sold')
    acc_sold.admin_order_field = '_acc_sold'


admin.site.register(UserReport, UserReportAdmin)


class ImagesInlineModelAdmin(admin.options.InlineModelAdmin):
    class Media:
        extra = '' if settings.DEBUG else '.min'
        js = [
            f'admin/js/vendor/jquery/jquery{extra}.js',
            'admin/js/jquery.init.js',
            'crm/js/dropzone.js',
            'crm/js/inline-dropzone.js',
        ]
        css = {
            "all": ('crm/css/dropzone.css',)
        }
    template = 'admin/crm/document/images-inline.html'


class DocumentImageInline(ImagesInlineModelAdmin):
    """Inline tabular form inside question add/edit admin interface."""
    model = DocumentImage
    extra = 0
    verbose_name = _('Image')
    verbose_name_plural = _('Images')
    fields = ('img', 'preview')
    readonly_fields = ('created_by', 'created_at', 'preview')

    def preview(self, obj):
        image = getattr(obj, 'img', None)
        url = image.url if image else ''
        return format_html('<a href="{url}" target="_blank"><img src="{url}" class="inline-image-itself"/></a>',
                url=url)
    preview.short_description = _('preview')
    preview.allow_tags = True


# class DocumentHistoryInline(admin.TabularInline):
#     model = DocumentStatusHistory
#     extra = 0
#     verbose_name = _('Add status')
#     verbose_name_plural = _('Add statuses')


class DocumentAdmin(admin.ModelAdmin):
    # Options for add/edit exact document page
    fieldsets = [
        (_('User'), {'fields': ('name', 'bet_country',)}),
        (_('Images'), {'fields': ('images_url',)}),
    ]
    readonly_fields = ('created_at', 'changed_at')
    save_as = False
    save_on_top = True
    inlines = (DocumentImageInline,)

    # Options for account list page
    list_display = ('id', 'changed_at', 'name', 'bet_country', 'listochek', 'listovshik', 'registrovshik', 'type')
    list_filter = (
        'name',
        # 'status',
        'bet_country',
        'listochek',
        'type',
        # ('statuses', RelatedDropdownFilter),
    )
    date_hierarchy = 'changed_at'
    list_display_links = ('id', 'name', 'changed_at')
    list_per_page = settings.ROWS_ON_EACH_PAGE
    search_fields = ('name',)
    ordering = ('-id', )
    actions = ('send_to_listovshik', 'send_to_registrovshik')
    actions_on_top = False
    actions_on_bottom = True
    view_on_site = False
    show_full_result_count = False

    # My custom options
    current_instance_id = None

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.current_instance_id = obj.id
        return super(DocumentAdmin, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        obj.owner = request.user
        super(DocumentAdmin, self).save_model(request, obj, form, change)

    def save_formset(self, request, form, formset, change):
        if 'document_images' in request.FILES:
            for afile in request.FILES.getlist('document_images'):
                print(afile)

        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            instance.user = request.user
            instance.save()
        formset.save_m2m()

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return [
                (_('User'), {'fields': ('name', 'bet_country',)}),
                (_('Images'), {'fields': ('images_url',)}),
            ]
        if request.user.is_superuser or request.user.has_perm('crm.change_document'):
            return [
                (_('User'), {'fields': ('name', 'bet_country',)}),
                (_('Document'), {'fields': (
                    'listochek', 'owner',
                    ('created_at', 'changed_at'),
                    ('registrovshik', 'listovshik'))}),
                (None, {'fields': ('account', 'images_url'),}),
            ]
        else:
            return [
                (_('User'), {'fields': ('name', 'bet_country',)}),
                (_('Document'), {'fields': (
                    'listochek',
                    # 'owner',
                    ('created_at', 'changed_at'),
                    ('registrovshik', 'listovshik'))}),
                (None, {'fields': ('account', 'images_url'),}),
            ]

        # return self.fieldsets + [(None, {'fields': ('account', 'images_url')}),]

    def get_readonly_fields(self, request, obj=None):
        fields = super(DocumentAdmin, self).get_readonly_fields(request, obj)
        fields = self._get_readonly_fields_by_permissions(obj, fields, request)
        return fields

    def _get_readonly_fields_by_permissions(self, obj, fields, request):
        user = request.user
        if not obj:  # On account add
            return fields  # + ('status', )
        if user.is_superuser:
            return fields
        else:
            fields = fields + ('owner', 'registrovshik', 'listovshik')
        return fields

    def get_queryset(self, request):
        queryset = super(DocumentAdmin, self).get_queryset(request)
        queryset = self._filter_queryset_by_permissions(queryset, request)
        return queryset

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super(DocumentAdmin, self).get_search_results(request, queryset, search_term)
        queryset = self._filter_queryset_by_permissions(queryset, request)
        return queryset, use_distinct

    def _filter_queryset_by_permissions(self, queryset, request):
        user = request.user
        if user.is_superuser or user.has_perm('crm.view_document'):
            return queryset
        if user.has_perm('crm.view_document_owned_only') \
                or user.has_perm('crm.view_document_if_listovshik') \
                or user.has_perm('crm.view_document_if_registrovshik'):
            filters = Q()
            if user.has_perm('crm.view_document_owned_only'):
                filters |= Q(owner=user.id)
            if user.has_perm('crm.view_document_if_listovshik'):
                filters |= Q(listovshik=user.id)
            if user.has_perm('crm.view_document_if_registrovshik'):
                filters |= Q(registrovshik=user.id)
            queryset = queryset.filter(filters)
        # if user.has_perm('crm.view_document_in_state_created'):
        #     queryset = queryset.filter(status=DocStatus.CREATED.value)
        # if user.has_perm('crm.view_document_in_state_listovshik'):
        #     queryset = queryset.filter(status=DocStatus.LISTOVSHIK.value)
        # if user.has_perm('crm.view_document_in_state_registrovshik'):
        #     queryset = queryset.filter(status=DocStatus.REGISTROVSHIK.value)
        return queryset

    def has_view_permission(self, request, obj=None):
        user = request.user
        owned_only = user.has_perm('crm.view_document_owned_only')
        user_listovshik = user.has_perm('crm.view_document_if_listovshik')
        user_registrovshik = user.has_perm('crm.view_document_if_registrovshik')
        # created = user.has_perm('crm.view_document_in_state_created')
        # st_listovshik = user.has_perm('crm.view_document_in_state_listovshik')
        # st_registrovshik = user.has_perm('crm.view_document_in_state_registrovshik')
        if obj:
            owned_only = owned_only and obj.owner == user.id
            user_listovshik = user_listovshik and obj.listovshik == user.id
            user_registrovshik = user_registrovshik and obj.registrovshik == user.id
            # created = created and obj.status == DocStatus.CREATED.value
            # st_listovshik = st_listovshik and obj.status == DocStatus.LISTOVSHIK.value
            # st_registrovshik = st_registrovshik and obj.status == DocStatus.REGISTROVSHIK.value
        return (super(DocumentAdmin, self).has_view_permission(request, obj)
                or owned_only or user_listovshik or user_registrovshik)
                # or created or st_listovshik or st_registrovshik)

    def has_change_permission(self, request, obj=None):
        user = request.user
        owned_only = user.has_perm('crm.change_document_owned_only')
        user_listovshik = user.has_perm('crm.change_document_if_listovshik')
        user_registrovshik = user.has_perm('crm.change_document_if_registrovshik')
        # created = user.has_perm('crm.change_document_in_state_created')
        # st_listovshik = user.has_perm('crm.change_document_in_state_listovshik')
        # st_registrovshik = user.has_perm('crm.change_document_in_state_registrovshik')
        if obj:
            owned_only = owned_only and obj.owner == user.id
            user_listovshik = user_listovshik and obj.listovshik == user.id
            user_registrovshik = user_registrovshik and obj.registrovshik == user.id
            # created = created and obj.status == DocStatus.CREATED.value
            # st_listovshik = st_listovshik and obj.status == DocStatus.LISTOVSHIK.value
            # st_registrovshik = st_registrovshik and obj.status == DocStatus.REGISTROVSHIK.value
        return (super(DocumentAdmin, self).has_change_permission(request, obj)
                or owned_only or user_listovshik or user_registrovshik)
                # created or st_listovshik or st_registrovshik

    def get_actions(self, request):
        """Conditional actions. Only superuser can delete accounts from database."""
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    def get_urls(self):
        custom_urls = [
            path('send-to-listovshik/', self.admin_site.admin_view(self.send_to_listovshik_intermediate),
                 name='crm_document_send_to_listovshik'),
            path('send-to-registrovshik', self.admin_site.admin_view(self.send_to_registrovshik_intermediate),
                 name='crm_document_send_to_registrovshik'),
            path('upload-images-ajax/', self.admin_site.admin_view(self.upload_images_ajax),
                name='crm_document_upload_images_ajax')
        ]
        return custom_urls + super(DocumentAdmin, self).get_urls()

    def upload_images_ajax(self, request, queryset=None):
        document_id = request.GET.get('document_id', None)
        user = request.user
        if request.FILES:
            for key, image_file in request.FILES.items():
                d_img = DocumentImage(img=image_file, created_by=user, document_id=document_id)
                d_img.save()
        return JsonResponse({})

    def send_to_listovshik(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        user = request.user
        url = reverse('admin:crm_document_send_to_listovshik') + '?ids={0}'.format(','.join(selected))
        return HttpResponseRedirect(url)

    def send_to_listovshik_intermediate(self, request):
        doc_ids = [int(d_id) for d_id in request.GET.get('ids', '').split(',') if d_id]
        user = request.user
        if request.method == 'POST' and 'user_to_sent' in request.POST:
            if user.is_superuser or user.has_perm('crm.send_document_to_listovshik'):
                user_id = int(request.POST['user_to_sent'])
                if user_id and doc_ids:
                    user = User.objects.get(pk=user_id)
                    documents = Document.objects.filter(id__in=doc_ids)
                    for doc in documents:
                        doc.listovshik = user
                        # if doc.status == DocStatus.CREATED.value:
                        #     doc.status = DocStatus.LISTOVSHIK.value
                        doc.save(update_fields=('listovshik',))
            return HttpResponseRedirect(reverse('admin:crm_document_changelist'))

        return render(request, 'admin/crm/document/send_to_user.html', {
            'users': User.objects.filter(groups__id=settings.GROUP_LISTOVSHIK_ID).order_by('last_name'),
            'documents': Document.objects.filter(id__in=doc_ids),
            'form_url': reverse('admin:crm_document_send_to_listovshik') + '?ids={0}'.format(request.GET.get('ids', '')),
        })
    send_to_listovshik.short_description = _('Send document to listovshik')
    send_to_listovshik.allowed_permissions = ('change', 'send_to_listovshik')

    def has_send_to_listovshik_permission(self, request):
        return request.user.has_perm('crm.send_document_to_listovshik')

    def send_to_registrovshik(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        url = reverse('admin:crm_document_send_to_registrovshik') + '?ids={0}'.format(','.join(selected))
        return HttpResponseRedirect(url)

    def send_to_registrovshik_intermediate(self, request):
        doc_ids = [int(d_id) for d_id in request.GET.get('ids', '').split(',') if d_id]
        user = request.user
        if request.method == 'POST' and 'user_to_sent' in request.POST:
            if user.is_superuser or user.has_perm('crm.send_document_to_listovshik'):
                user_id = int(request.POST['user_to_sent'])
                if user_id and doc_ids:
                    user = User.objects.get(pk=user_id)
                    documents = Document.objects.filter(id__in=doc_ids)
                    for doc in documents:
                        doc.registrovshik = user
                        # if doc.status == DocStatus.NEED_REGISTROVSHIK.value:
                        #     doc.status = DocStatus.REGISTROVSHIK.value
                        doc.save(update_fields=('registrovshik', ))
            return HttpResponseRedirect(reverse('admin:crm_document_changelist'))
        return render(request, 'admin/crm/document/send_to_user.html', {
            'users': User.objects.filter(groups__id=settings.GROUP_REGISTROVSHIK_ID).order_by('last_name'),
            'documents': Document.objects.filter(id__in=doc_ids),
            'form_url': reverse('admin:crm_document_send_to_registrovshik') + '?ids={0}'.format(request.GET.get('ids', '')),
        })
    send_to_registrovshik.short_description = _('Send document to registrovshik')
    send_to_registrovshik.allowed_permissions = ('change', 'send_to_registrovshik')

    def has_send_to_registrovshik_permission(self, request):
        return request.user.has_perm('crm.send_document_to_registrovshik')

    def _get_instance_id(self, request=None):
        if not self.current_instance_id and request:
            self.current_instance_id = int(resolve(request.path).kwargs['object_id'])
        return self.current_instance_id

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        if db_field.choices:
            return self.formfield_for_choice_field(db_field, request, **kwargs)

        if isinstance(db_field, (models.ForeignKey, models.ManyToManyField)):
            if db_field.name == 'account':
                object_id = self._get_instance_id(request)
                queryset = Account.objects.filter(Q(document__id=object_id) | Q(document__id=None))
                if not request.user.is_superuser:
                    queryset = queryset.filter(created_by=request.user.id)
                kwargs['queryset'] = queryset

            if db_field.__class__ in self.formfield_overrides:
                kwargs = {**self.formfield_overrides[db_field.__class__], **kwargs}
            formfield = None
            if isinstance(db_field, models.ForeignKey):
                formfield = self.formfield_for_foreignkey(db_field, request, **kwargs)
            elif isinstance(db_field, models.ManyToManyField):
                formfield = self.formfield_for_manytomany(db_field, request, **kwargs)

            if formfield and db_field.name not in self.raw_id_fields:
                related_modeladmin = self.admin_site._registry.get(db_field.remote_field.model)
                wrapper_kwargs = {}
                if related_modeladmin:
                    wrapper_kwargs.update(
                        can_add_related=related_modeladmin.has_add_permission(request),
                        can_change_related=related_modeladmin.has_change_permission(request),
                        can_delete_related=related_modeladmin.has_delete_permission(request),
                        can_view_related=related_modeladmin.has_view_permission(request),
                    )
                if db_field.name == 'account':
                    wrapper_kwargs['object_id'] = object_id
                    formfield.widget = AccountAddWidget(
                        formfield.widget, db_field.remote_field, self.admin_site, **wrapper_kwargs
                    )
                else:
                    formfield.widget = RelatedFieldWidgetWrapper(
                        formfield.widget, db_field.remote_field, self.admin_site, **wrapper_kwargs
                    )
            return formfield

        for klass in db_field.__class__.mro():
            if klass in self.formfield_overrides:
                kwargs = {**copy.deepcopy(self.formfield_overrides[klass]), **kwargs}
                return db_field.formfield(**kwargs)
        return db_field.formfield(**kwargs)


admin.site.register(Document, DocumentAdmin)
