if (!$) {$ = django.jQuery;}

(function($) {
//'use strict';

console.log('inside images-inline js script');

function createPreview(fileObj) {
	var src = URL.createObjectURL(fileObj);
	return $('<div>', {
		class: 'inline-image-element inline-image-preview'
	}).append($('<img>', {src: src, class: 'inline-image-itself'}));
}

$(function () {
	var $imagesWrapper = $('#inline-images-wrapper');

	$(document).on({
		'dragover': function(e) { e.preventDefault(); },
		'drop': function (e) { e.preventDefault(); }
	});

	$imagesWrapper.on({
		'dragover': function(e) {
			e.preventDefault();
			e.stopPropagation();
		},
		'dragleave': function(e) {
			e.preventDefault();
			e.stopPropagation();
		},
		'drop':function(e) {
			if (e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length) {
				var $wrapper = $(e.target);
				if (!$wrapper.hasClass('inline-image-input-wrapper')) {
					$wrapper = $imagesWrapper.find('.inline-image-input-wrapper:visible:first');
					//$wrapper = $wrapper.parents('.inline-image-element');
				}
				var $input = $wrapper.find('input[type=file]:first');
				//$input.prop('files', e.dataTransfer.files);
				$input.prop('files', e.originalEvent.dataTransfer.files);
				onImageInputChange($input, $wrapper, $imagesWrapper);
			}
			e.preventDefault();
			e.stopPropagation();
		}
	});

	function traverseFileTree(item, $wrapper, path) {
		path = path || '';
		if (item.isFile) {
			// Get file
			item.file(function(file) {
				console.log("File:", path + file.name);
				var $preview = createPreview(f);
				$preview.insertBefore($wrapper);
			});
		} else if (item.isDirectory) {
			// Get folder contents
			var dirReader = item.createReader();
			dirReader.readEntries(function(entries) {
				for (var i=0; i < entries.length; i++) {
					traverseFileTree(entries[i], $wrapper, path + item.name + '/');
				}
			});
		}
	}

	function onImageInputChange($input, $wrapper, $allWrapper) {
		var counter = Number.parseInt($input.attr('data-counter')) + 1;
		var files = $input.prop('files');
		console.log('input:', $input, 'files:', files);
		for (var i in files) { if (files.hasOwnProperty(i)) {
			var f = files[i];
			var item = f.webkitGetAsEntry? f.webkitGetAsEntry(): null;
			if (item) {
				traverseFileTree(item, $wrapper);
			} else {
				var $preview = createPreview(f);
				$preview.insertBefore($wrapper);
			}
		}}
		$wrapper.hide();
		$('<div class="inline-image-element inline-image-input-wrapper">\n' +
			'\t<input type="file" id="inline-image-input-'+counter+'" multiple accept="image/*" style="display:none" name="document_images" data-counter='+counter+'>\n' +
			'\t<label for="inline-image-input-'+counter+'" class="inline-image-label" title="Добавить картинку">&nbsp;</label>\n' +
			'</div>').appendTo($allWrapper);
	}

	$imagesWrapper.on('change', 'input[type=file]', function(e) {
		var $input = $(e.target),
			$wrapper = $input.parent();
		onImageInputChange($input, $wrapper, $imagesWrapper);
	});

	// 		counter = Number.parseInt($input.attr('data-counter')) + 1;
	// 	for (var i in e.target.files) { if(e.target.files.hasOwnProperty(i)) {
	// 		$preview = createPreview(e.target.files[i]);
	// 		$preview.insertBefore($wrapper);
	// 	}}
	// 	$wrapper.hide();
	// 	$('<div class="inline-image-element" id="inline-image-input-wrapper">\n' +
	// 		'\t<input type="file" id="inline-image-input-'+counter+'" multiple accept="image/*" style="display:none" name="document_images" data-counter='+counter+'>\n' +
	// 		'\t<label for="inline-image-input-'+counter+'" class="inline-image-label" title="Добавить картинку">&nbsp;</label>\n' +
	// 		'</div>').appendTo($imagesWrapper);
});

})($);
