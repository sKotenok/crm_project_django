if (!$) {$ = django.jQuery;}
Dropzone.autoDiscover = false;

(function($) {
	$(function () {
		var csrf = $('input[type=hidden][name=csrfmiddlewaretoken]:first').val();
		var myDropzone = new Dropzone('#dropzone', {
			//url: '/admin/crm/document/upload-images-ajax/',
			url: window.IMAGES_INLINE_CONFIG.url + '?document_id='+window.IMAGES_INLINE_CONFIG.document_id,
			paramName: 'files',
			uploadMultiple: true,
			createImageThumbnails: true,
			thumbnailHeight: 400,
			thumbnailMethod: 'contain',
			acceptedFiles: 'image/*',
			autoProcessQueue: true,
			addRemoveLinks: false,
			forceFallback: false,
			headers: {
				'X-CSRFToken': csrf
			},
			dictDefaultMessage: 'Перетащите файл или папку для добавления',
			dictFallbackMessage: 'Ваш браузер не поддерживается',
			dictFallbackText: 'Ваш браузер слишком старый или IE',
			dictFileTooBig: 'Файл слишком большой',
			dictInvalidFileType: "Недопустимый тип файла, разрешены только изображения.",
			dictResponseError: "Ошибка сервера. {{statusCode}}",
			dictRemoveFile: "Удалить",
			dictRemoveFileConfirmation: "Точно удалить?",
		});

	});
})($);
