import os
from mimetypes import guess_type
from django.conf import settings
from templated_email import send_templated_mail

# import django.core.mail.message


def send_mail_to_customer(acc_selling, accounts, request):
    if not settings.ACC_EMAIL_SEND:
        print('--- SENDING EMAIL ---')
        print(acc_selling)
        for acc in accounts: print(acc)
        print('--- END OF EMAIL ---')
    else:
        attachment = get_email_attachment(settings.ACC_EMAIL_FILE) if settings.ACC_EMAIL_FILE else None
        print(attachment[0], attachment[2])

        send_templated_mail(
                template_name='account-selling',
                from_email=settings.ACC_EMAIL_FROM,
                recipient_list=[acc_selling.customer_email],
                context={
                    'acc_selling': acc_selling,
                    'accounts': accounts,
                    'footer': settings.ACC_EMAIL_FOOTER
                },
                attachments=[attachment] if attachment else None
        )
    return True


def get_email_attachment(filename):
    attachment = None
    filepath = os.path.join(settings.STATIC_ROOT, filename)
    filename = os.path.basename(filepath)
    print('file:', filepath)
    if os.path.isfile(filepath):
        mimetype = guess_type(filepath)[0]
        with open(filepath, 'rb') as fp:
            attachment = [filename, fp.read(), mimetype]
    return attachment
