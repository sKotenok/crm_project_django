from datetime import datetime
from django.db import models
from django.db.models import fields, Lookup
from django.utils.text import format_lazy
from django.utils.translation import gettext_lazy as _
from django.utils.timezone import now as timezone_now
from django.conf import settings
from django.core.exceptions import ValidationError

from .choice_fields import (Status, DocType,
        STATUS_CHOICES, COUNTRY_CHOICES, CURRENCY_CHOICES, DOCUMENT_TYPE_CHOICES)
# , DocStatus, DOCUMENT_STATUS_CHOICES, )


@fields.Field.register_lookup
class NotEqual(Lookup):
    lookup_name = 'not'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '{0} <> {1}'.format(lhs, rhs), params


class Account(models.Model):
    status = models.PositiveSmallIntegerField(_('Status'), choices=STATUS_CHOICES, default=1)
    mail_email = models.EmailField(_('Email'), max_length=200, blank=True, default='')
    mail_name = models.CharField(_('Mail: name'), max_length=200, blank=True, default='')
    mail_phone = models.CharField(_('Mail: phone'), max_length=200, blank=True, default='')
    mail_login = models.CharField(_('Mail: login'), max_length=200, blank=True, default='')
    mail_password = models.CharField(_('Mail: password'), max_length=200, blank=True, default='')
    mail_question = models.CharField(_('Mail: question'), max_length=200, blank=True, default='')

    neteller_id = models.CharField(_('Neteller: id'), max_length=200, blank=True, default='')
    neteller_email = models.EmailField(_('Neteller: email'), max_length=200, blank=True, default='',
        help_text=format_lazy(_('Live this field empty to autofill from {0}'), _('Email')))
    neteller_login = models.CharField(_('Neteller: login'), max_length=200, blank=True, default='')
    neteller_password = models.CharField(_('Neteller: password'), max_length=200, blank=True, default='')
    neteller_code = models.CharField(_('Neteller: code'), max_length=200, blank=True, default='')

    bet_name = models.CharField(_('Bet: name'), max_length=200, blank=True, default='',
        help_text=format_lazy(_('Live this field empty to autofill from {0}'), _('Mail: name')))
    bet_date = models.DateField(_('Bet: date'), blank=True, default=timezone_now)
    bet_login = models.CharField(_('Bet: login'), max_length=200, blank=True, default='')
    bet_password = models.CharField(_('Bet: password'), max_length=200, blank=True, default='')
    bet_pin = models.CharField(_('Bet: pin'), max_length=200, blank=True, default='')
    bet_country = models.CharField(_('Bet: country'), max_length=5, choices=COUNTRY_CHOICES, default='ru')
    bet_currency = models.CharField(_('Currency'), max_length=10, choices=CURRENCY_CHOICES, default='dollars')

    proxy_ip = models.CharField(_('IP'), max_length=100, blank=True, default='')
    proxy_port = models.IntegerField(_('Port'), default=8080, blank=True)
    proxy_proxy = models.CharField(_('Proxy'), max_length=200, blank=True, default='')
    proxy_login = models.CharField(_('Login'), max_length=200, blank=True, default='')
    proxy_password = models.CharField(_('Password'), max_length=200, blank=True, default='')
    proxy_created_at = models.DateField(_('Proxy: Time created'), default=timezone_now, blank=True)
    proxy_duration = models.IntegerField(_('Proxy: duration'), default=settings.PROXY_DURATION)

    notes = models.TextField(_('Notes'), blank=True, default='')
#    doc_href = models.CharField(_('Document href'), max_length=500, blank=True, default='')
#    doc_href = models.OneToOneField('Document', on_delete=models.SET_NULL)
#    doc_date = models.DateField(_('Document date'), default=timezone_now, blank=True)
#    document = models.OneToOneField('Document', on_delete=models.SET_NULL, related_name='my_account',
#                                   null=True, verbose_name=_('Document'), blank=True)

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
            null=True, verbose_name=_('Created by'))
    created_at = models.DateTimeField(_('Time created'), auto_now_add=True)

    class Meta:
        verbose_name = _('Account')
        verbose_name_plural = _('Accounts')
        permissions = (
            ('change_account_owned_only', _('Change his own accounts only')),
            ('change_account_not_sold', _('Change accounts not in state: sold')),
            ('change_account_at_verification_only', _('Change accounts in state: verification')),
            ('cant_change_account_status', _('Can not change account status')),
            ('view_account_owned_only', _('View his own accounts')),
            ('view_account_not_sold', _('View accounts not in state: sold')),
        )

    def __str__(self):
        return '{0} {1} {2} {3}'.format(self.mail_email, self.mail_name, self.status, self.bet_country)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.neteller_email:     self.neteller_email = self.mail_email
        if not self.bet_name:           self.bet_name = self.mail_name
        return super(Account, self).save(force_insert=force_insert, force_update=force_update,
                                  using=using, update_fields=update_fields)


class AccountStatusHistory(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    old_status = models.PositiveSmallIntegerField(_('Old status'))
    new_status = models.PositiveSmallIntegerField(_('New status'))

    changed_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    changed_at = models.DateTimeField(_('Was changed at'), auto_now_add=True)

    def __str__(self):
        return '{0} {1} {2} {3}'.format(self.account, self.old_status, self.new_status, self.changed_at)


class AccountSelling(models.Model):
    customer_email = models.EmailField(_('Customer email'))
    count = models.PositiveIntegerField(_('Count of accounts to sent'), blank=False, default=1)
    bet_country = models.CharField(_('Bet: country'), max_length=5, choices=COUNTRY_CHOICES, default='ru')
    currency = models.CharField(_('Currency'), max_length=10, choices=CURRENCY_CHOICES, default='dollars')

    sended = models.BooleanField(_('Sended'), default=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,
            null=True, verbose_name=_('Created by'))
    created_at = models.DateTimeField(_('Time created'), auto_now_add=True)

    accounts = models.ManyToManyField(Account, through='SoldAccount')

    class Meta:
        verbose_name = _('Account Selling')
        verbose_name_plural = _('Account Sellings')

    def __str__(self):
        return '{0} {1} {2} {3}'.format(self.customer_email, self.count, self.bet_country, self.currency)

    def clean(self):
        super(AccountSelling, self).clean()

        available_count = Account.objects.filter(
            status=Status.READY,
            bet_currency=self.currency,
            bet_country=self.bet_country
        ).count()

        if self.count > available_count or self.count <= 0:
            msg = _('You are trying to sell %(sell_count)s accounts. There are only %(real_count)s available. Try again.') \
                        % {'sell_count': str(self.count), 'real_count': str(available_count)}
            raise ValidationError(msg)


class SoldAccount(models.Model):
    account = models.ForeignKey(Account, verbose_name=_('Account'), on_delete=models.CASCADE)
    selling = models.ForeignKey(AccountSelling, verbose_name=_('Selling'), on_delete=models.CASCADE)


# --- DOCUMENT ---
class Document(models.Model):
    name = models.CharField(_('Mail: name'), max_length=200, blank=True, default='')
    bet_country = models.CharField(_('Bet: country'), max_length=5, choices=COUNTRY_CHOICES, default='ru')
    listochek = models.BooleanField(_('Listochek'), default=False, null=False)
    # status = models.PositiveSmallIntegerField(_('Status'), choices=DOCUMENT_STATUS_CHOICES, default=DocStatus.CREATED.value)
    type = models.PositiveSmallIntegerField(_('Type'), choices=DOCUMENT_TYPE_CHOICES, default=DocType.ORIGINAL.value)
    images_url = models.CharField(_('Images URL'), default=None, max_length=500, blank=True, null=True)

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,
            verbose_name=_('Owner'))
    registrovshik = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True,
            verbose_name=_('Registrovshik'), related_name='registerovshik_in_documents')
    listovshik = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True,
            verbose_name=_('Listovshik'), related_name='listovshik_in_documents')
    account = models.OneToOneField(Account, on_delete=models.SET_NULL, related_name='document',
                                   null=True, verbose_name=_('Account'), blank=True)
    created_at = models.DateTimeField(_('Time created'), auto_now_add=True)
    changed_at = models.DateTimeField(_('Time changed'), auto_now=True)

    class Meta:
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')
        permissions = (
            ('view_document_owned_only', _('View his own documents')),
            ('view_document_if_listovshik', _('View document if listovshik')),
            ('view_document_if_registrovshik', _('View document if registrovshik')),
            ('change_document_owned_only', _('Change his own documents only')),
            ('change_document_if_listovshik', _('Change document if listovshik')),
            ('change_document_if_registrovshik', _('Change document if registrovshik')),
            # ('change_document_status', _('Change document status')),
            ('send_document_to_listovshik', _('Send document to listovshik')),
            ('send_document_to_registrovshik', _('Send document to registrovshik')),
        )

    def __str__(self):
        return '{0}. {1} ({2}) - {3}'.format(self.id, self.name, self.bet_country, self.owner)

    # def __init__(self, *args, **kwargs):
    #     super(Document, self).__init__(*args, **kwargs)
    #     self.__previous_status = self.status

    # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    #     super(Document, self).save(force_insert, force_update, using, update_fields)
    #     if self.__previous_status != self.status:
    #         # status was changed
    #         new_status = DocumentStatusHistory(
    #             status=self.status, document=self,
    #             changed_by=self.owner, changed_at=self.changed_at)
    #         new_status.save()


# class DocumentStatusHistory(models.Model):
#     status = models.PositiveSmallIntegerField(_('Status'), choices=DOCUMENT_STATUS_CHOICES, default=DocStatus.CREATED.value)
#
#     document = models.ForeignKey(Document, on_delete=models.CASCADE, verbose_name=_('Document'), related_name='statuses')
#     changed_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, verbose_name=_('Changed by'))
#     changed_at = models.DateTimeField(_('Time changed'), auto_now=True)
#
#     class Meta():
#         verbose_name = _('Document')
#         verbose_name_plural = _('Documents')


def custom_image_path(instance, filename):
    return 'images/{0}/{1}/{2}'.format(datetime.now().year, str(instance.document_id), filename)


class DocumentImage(models.Model):
    img = models.ImageField(_('Image'), upload_to=custom_image_path)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, verbose_name=_('Uploaded by'))
    created_at = models.DateTimeField(_('Time created'), auto_now_add=True)
    document = models.ForeignKey(Document, on_delete=models.CASCADE, verbose_name=_('Document'))

    def __str__(self):
        return str(self.img)


from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver


@receiver(post_delete, sender=DocumentImage)
def photo_post_delete_handler(sender, instance=None, **kwargs):
    if instance and instance.img:
        storage, path = instance.img.storage, instance.img.path
        if storage and path:
            storage.delete(path)

