# Email config (Настройки почтового сервера)
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'chernbiykot@yandex.ru'
EMAIL_HOST_PASSWORD = 'Rh0d0gbqwf'
# EMAIL_USE_TLS = True ## TLS или SSL - зависит от почтового сервера.
EMAIL_USE_SSL = True


# Account email sending
ACC_EMAIL_SEND = True  # True|False - если False, почта отправляться не будет.
ACC_EMAIL_FROM = 'chernbiykot@yandex.ru'  # Поле FROM в письме (для yandex-а должно совпадать с EMAIL_HOST_USER
ACC_EMAIL_SUBJECT = 'Получите, распишитесь'  # Заголовок письма
ACC_EMAIL_FOOTER = """

Эй, слышь, купи слона!
chernbiykot@yandex.ru

"""  # Подпись в письме (контакты и т.п.)
ACC_EMAIL_FILE = 'email_files/Cheklist.pdf'  # pdf-файл для отправки во вложении письма.


# Proxy column config (Настройки колонки "осталось жить прокси серверу")
PROXY_DURATION = 20         # Сколько дней осталось жить проки-серверу по умолчанию
PROXY_DURATION_WARNING = 10 # За сколько дней до конца подкрашивать прокси желтым
PROXY_DURATION_CRITICAL = 2 # За сколько дней подкрашивать прокси красным


ROWS_ON_EACH_PAGE = 50 # Количество строк на одной странице в таблицах (до появления пагинации)


GROUP_LISTOVSHIK_ID = 4     # ID группы "Листовщики", нужно для ограничения пользователей на странице "Назначьте листовщика"
GROUP_REGISTROVSHIK_ID = 2  # ID группы "Регистровщики"